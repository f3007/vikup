<?php
namespace App\Services\Api;

use App\Http\Controllers\ProfileController;
use App\Http\Requests\EventsStoreRequest;
use App\Http\Requests\EventsUpdateRequest;
use App\Http\Resources\EventsResource;
use App\Http\Resources\ProfileResource;
use App\Http\Traits\Imagable;
use App\Models\Events;
use App\Models\User;
use App\Models\UserEvents;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class EventsService {

    use Imagable;

    public function index(Request $request) {
        return Events::all();
    }

    public function show(Request $request, $id) {
        return Events::find($id);
    }

    public function store(EventsStoreRequest $storeRequest) {
        $organizer = User::find(auth('api')->user()->id);
        $data = $storeRequest->validated();
        $file = $this->fromBase64($data['img']);
        Storage::disk(config('voyager.storage.disk'))->put('',$file);
        Events::create([
            'organizer_id' => $organizer->id,
            'title' => $data['title'],
            'description' => $data['description'],
            'date' => Carbon::parse($data['date']),
            'price' => $data['price'],
            'address' => $data['address'],
            'start_time' => $data['start_time'],
            'end_time' => $data['end_time'],
            'img' => $file->hashName()
        ]);
        return response()->json(['message' => 'Событие создано!']);
    }

    public function update(EventsUpdateRequest $updateRequest, $id) {
        $organizer = User::find(auth('api')->user()->id);
        $data = $updateRequest->validated();
        $event = Events::find($id);
        $file = $this->fromBase64($data['img']);
        Storage::disk(config('voyager.storage.disk'))->delete('',$event->img);
        Storage::disk(config('voyager.storage.disk'))->put('',$file);
        $event->update([
            'organizer_id' => $organizer->id,
            'title' => $data['title'],
            'description' => $data['description'],
            'date' => Carbon::parse($data['date']),
            'price' => $data['price'],
            'address' => $data['address'],
            'start_time' => $data['start_time'],
            'end_time' => $data['end_time'],
            'img' => $file->hashName()
        ]);
        return response()->json(['message' => 'Событие изменено!']);
    }

    public function destroy(Request $request, $id) {
        $organizer = User::find(auth('api')->user()->id);
        $event = $this->show($request, $id);
        $event->delete();
        return response()->json(['message' => 'Событие удалено!']);
    }

    public function register(Request $request, $id) {
        $event = Events::find($id);
        $dancer = User::find(auth('api')->user()->id);
        $user_event = UserEvents::where('event_id', $id)
                                ->where('dancer_id', $dancer->id)->first();
        if ($user_event) {
            return response()->json(['message' => 'Вы уже зарегистрированы на событие!']);
        }
        UserEvents::create([
            'dancer_id' => $dancer->id,
            'event_id' => $event->id
        ]);
        return response()->json(['message' => 'Вы зарегистрированы на событие']);
    }

    public function cancelRegistration(Request $request, $id) {
        $event = Events::find($id);
        $dancer = User::find(auth('api')->user()->id);
        $user_event = UserEvents::where('event_id', $event->id)
            ->where('dancer_id', $dancer->id)->first();
        $user_event->delete();
        return response()->json(['message' => 'Вы отменили регистрацию на событие']);
    }

    public function myEvents(Request $request) {
        $dancer = User::find(auth('api')->user()->id);
        $event_ids = UserEvents::all()->where('user_id', $dancer->id)->pluck('event_id');
        return EventsResource::collection(Events::all()->whereIn('id', $event_ids));
    }

    public function registeredToEvent(Request $request, $id) {
        $organizer = User::find(auth('api')->user()->id);
        $user_ids = UserEvents::all()->where('event_id', $id)->pluck('dancer_id');
        return User::all()->whereIn('id', $user_ids);
    }
}
