<?php

namespace App\Services\Auth;

use App\Http\Requests\SocialMediaLoginRequest;
use App\Http\Traits\Hashable;
use App\Models\User;
use Faker\Factory;
use Laravel\Socialite\Facades\Socialite;

class SocialMediaLoginService extends TokenIssueService
{
    use Hashable;

    private function createUser(SocialMediaLoginRequest $request) {
        $factory = Factory::create();
        $user = User::create([
            'email' => $factory->email,
            $request['provider'] => $this->getUserFromToken($request)->id,
            'password' => $this->hashPassword($factory->password)
        ]);
        return $this->generateRegistrationTokens($user);
    }

    private function getUserFromToken(SocialMediaLoginRequest $request) {
        $data = $request->validated();
        return Socialite::driver($data['provider'])->userFromToken($data['token']);
    }

    public function socialMediaLogin(SocialMediaLoginRequest $request) {
        $user_from_token = $this->getUserFromToken($request);
        $user = User::where($request['provider'], $user_from_token->id)->first();
        if (empty($user)) {
            return $this->createUser($request);
        }
        return $this->generateRegistrationTokens($user);
    }
}
