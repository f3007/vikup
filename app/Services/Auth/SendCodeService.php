<?php

namespace App\Services\Auth;

use App\Mail\ForgotPassword;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use function env;

abstract class SendCodeService
{
    public function sendEmail($email, $code)
    {
        Mail::to($email)->send(new ForgotPassword($code));
    }

    public function sendSms($phone_number, $code)
    {
        Http::get('https://smsc.kz/sys/send.php?login=' . env('SMS_SERVICE_LOGIN') . '&psw=' . env('SMS_SERVICE_PASSWORD') . '&phones=' . $phone_number . '&mes=Бартер-Маркетплейс: ' . $code);
    }
}
