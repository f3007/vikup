<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventsStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'date' => 'required|date',
            'price' => 'required|string',
            'address' => 'required|string',
            'start_time' => 'required|string',
            'end_time' => 'required|string',
            'img' => 'required|string'
        ];
    }
}
