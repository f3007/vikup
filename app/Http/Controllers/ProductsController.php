<?php

namespace App\Http\Controllers;

use App\Http\Requests\MakeProductRequests;
use App\Http\Resources\ProductsResource;
use App\Models\BuyersRequests;
use App\Models\HasSellers;
use App\Models\Products;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function makeRequests(MakeProductRequests $makeProductRequests) {
        $data = $makeProductRequests->validated();
        $product = Products::where('title', $data['title'])
                ->where('price_from', $data['price_from'])
                ->where('price_to', $data['price_to'])
                ->where('type', $data['type'])->first();
        if (!$product) {
            return response()->json(['message' => 'Продукт не найден!']);
        }
        BuyersRequests::create([
            'buyer_id' => auth('api')->user()->id,
            'product_id' => $product->id
        ]);
        return response()->json(['message' => 'Запрос создан!']);
    }

    public function showOwnRequests(Request $request) {
        $buyer = auth()->user()->id;
        $all_requests = BuyersRequests::all()->where('buyer_id', $buyer->id)->pluck('product_id');
        return ProductsResource::collection(Products::all()->whereIn('id', $all_requests));
    }

    public function showMyProductRequests(Request $request) {
        $seller = auth()->user()->id;
        $all_products = HasSellers::all()->where('seller_id', $seller->id)->pluck('product_id');
        return ProductsResource::collection(Products::all()->whereIn('id', $all_products));
    }
}
