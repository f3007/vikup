<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class BuyersRequests extends Model
{
    use HasFactory;

    public function product(): BelongsToMany {
        return $this->belongsToMany(Products::class, 'buyers_requests')->withTimestamps();
    }

    public function buyer(): BelongsTo {
        return $this->belongsTo(User::class, 'buyer_id');
    }

    protected $fillable = [
        'buyer_id',
        'product_id'
    ];
}
