<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class HasSellers extends Model
{
    use HasFactory;

    public function product(): BelongsTo {
        return $this->belongsTo(Products::class, 'product_id');
    }

    public function sellers(): BelongsToMany {
        return $this->belongsToMany(User::class, 'has_sellers')->withTimestamps();
    }

    protected $fillable = [
        'seller_id',
        'product_id'
    ];
}
