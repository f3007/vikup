<?php

use App\Http\Controllers\ProductsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => ['api'],
    'prefix' => 'auth'
], function ($router) {
    Route::post('register', [RegistrationController::class, 'register']);
    Route::post('login', [LoginController::class, 'login']);
    Route::get('profile', [ProfileController::class, 'get']);
    Route::put('profile', [ProfileController::class, 'put']);
    Route::put('profile/change-password', [ProfileController::class, 'changePassword']);
    Route::delete('profile', [ProfileController::class, 'delete']);
    Route::post('logout', [ProfileController::class, 'logout']);
});

Route::middleware(['auth.jwt','role:buyer'])->group(function () {
    Route::post('make-request', [ProductsController::class, 'makeRequests']);
    Route::get('show-own-requests', [ProductsController::class, 'showOwnRequests']);
});

Route::middleware(['auth.jwt','role:seller'])->group(function () {
    Route::post('show-my-product-requests', [ProductsController::class, 'showMyProductRequests']);
});
