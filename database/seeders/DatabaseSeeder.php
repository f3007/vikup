<?php

namespace Database\Seeders;

use App\Models\BuyersRequests;
use App\Models\HasSellers;
use App\Models\Products;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->truncateVoyagerSeed();
        $this->callAppTables();
        $this->callSystemTables();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    private function truncateVoyagerSeed()
    {
        DB::table('menu_items')->truncate();
        DB::table('settings')->truncate();
        DB::table('permissions')->truncate();
        DB::table('permission_role')->truncate();
        DB::table('menus')->truncate();
        DB::table('data_rows')->truncate();
        DB::table('data_types')->truncate();
    }

    public function callSystemTables() {
        $this->call([
            DataTypesTableSeeder::class,
            DataRowsTableSeeder::class,
            MenusTableSeeder::class,
            MenuItemsTableSeeder::class,
            RolesTableSeeder::class,
            PermissionsTableSeeder::class,
            PermissionRoleTableSeeder::class,
            SettingsTableSeeder::class,
        ]);
    }

    public function callAppTables() {
        User::factory()->count(5)->create();
        $sellers = User::all();
        Products::factory()->count(7)->create();
        $products = Products::all();
        HasSellers::factory()->count(7)->create();
//        $has_sellers = HasSellers::all()->each(function ($product) use ($sellers) {
//            $product->sellers()->sync($sellers->random(3));
//        });
        BuyersRequests::factory()->count(7)->create();
//        $buyers = User::all();
//        $buyers_requests = BuyersRequests::all()->each(function ($request) use ($products) {
//            $request->products()->sync($products->random(3));
//        });
    }
}
