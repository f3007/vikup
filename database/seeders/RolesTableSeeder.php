<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $role = Role::firstOrNew(['name' => 'admin']);
        if (!$role->exists) {
            $role->fill([
                'display_name' => 'Администратор',
            ])->save();
        }

        $role = Role::firstOrNew(['name' => 'buyer']);
        if (!$role->exists) {
            $role->fill([
                'display_name' => 'Покупатель',
            ])->save();
        }

        $role = Role::firstOrNew(['name' => 'seller']);
        if (!$role->exists) {
            $role->fill([
                'display_name' => 'Продавец',
            ])->save();
        }
    }
}
