<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->title(),
            'price_from' => $this->faker->numberBetween(1000, 100000),
            'price_to' => $this->faker->numberBetween(100001, 250000),
            'type' => $this->faker->randomElement(['новый', 'б/у']),
        ];
    }
}
